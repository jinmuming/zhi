package com.free4inno.kmstika;

import com.free4inno.kmstika.dao.CronDao;
import com.free4inno.kmstika.service.AttachmentTikaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

/**
 * Author HUYUZHU.
 * Date 2021/3/27 21:40.
 */

@Configuration
@EnableScheduling
public class ScheduleTaskConfiguration implements SchedulingConfigurer  {

    @Autowired
    private CronDao cronDao;

    @Autowired
    AttachmentTikaService service;

    /**
     * 执行定时任务.
     */
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(
                /* 添加任务内容 */
                () -> service.parseAll(),
                /* 设置执行周期 */
                triggerContext -> {
                    /* 从数据库获取执行周期 */
                    String cron = cronDao.findByCronId(1).getTikaCron();
                    /* 返回执行周期 */
                    return new CronTrigger(cron).nextExecutionTime(triggerContext);
                }
        );
    }

}
