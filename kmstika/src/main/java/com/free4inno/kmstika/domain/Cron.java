package com.free4inno.kmstika.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * Author HUYUZHU.
 * Date 2021/3/27 21:44.
 */

@Entity
@Table(name = "cron")
@Data
public class Cron {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cron_id")
    private Integer cronId;

    @Column(name = "cron")
    private String cron;

    @Column(name = "tikaCron")
    private String tikaCron;

}
