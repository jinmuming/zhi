package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.SpecResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Author: HaoYi.
 * Date: 2021/02/07.
 */
@Repository
public interface SpecResourceDAO extends JpaRepository<SpecResource, Integer> {
    Optional<SpecResource> findByResourceIdAndType(Integer resourceId, Integer type);

    @Transactional
    void deleteByResourceIdAndType(Integer resourceId, Integer type);

    @Transactional
    void deleteByResourceId(Integer id);

    List<SpecResource> findAllByType(Integer type);

    List<SpecResource> findByResourceId(int id);
}
