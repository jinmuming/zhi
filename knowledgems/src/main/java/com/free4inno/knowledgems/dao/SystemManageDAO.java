package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.SystemManage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Author: ZhaoHaotian.
 * Date: 2021/04/24.
 */
@Repository
public interface SystemManageDAO extends JpaRepository<SystemManage, Integer> {
    Optional<SystemManage> findAllByVariable(String variable);

}
