package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.Cron;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Author LIYUNZE.
 * Date 2021/3/19.
 */

@Repository
public interface CronDAO extends JpaRepository<Cron, Integer> {
    Cron findByCronId(Integer cronId);
}
