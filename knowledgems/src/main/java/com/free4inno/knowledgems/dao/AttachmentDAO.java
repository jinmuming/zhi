package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Author HUYUZHU.
 * Date 2021/3/19 13:58.
 */

@Repository
public interface AttachmentDAO extends JpaRepository<Attachment, Integer> {

    List<Attachment> findAllByResourceId(Integer resourceId);

    @Transactional
    void deleteByUrl(String url);
}
