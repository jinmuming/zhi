package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Repository
public interface UserGroupDAO extends JpaRepository<UserGroup, Integer> {
    Optional<UserGroup> findById(Integer id);

    List<UserGroup> findByUserId(Integer userId);

    List<UserGroup> findAllByUserId(Integer userId);

    List<UserGroup> findByGroupId(Integer groupId);

    Optional<UserGroup> findByGroupIdAndUserId(Integer groupId, Integer userId);

    Optional<UserGroup> findByGroupIdAndPermission(Integer groupId, Integer permission);

    Boolean existsByGroupIdAndUserId(Integer groupId, Integer userId);

    @Transactional
    Integer deleteByGroupIdAndUserId(Integer groupId, Integer userId);

    @Transactional
    void deleteAllByGroupId(Integer groupId);

    @Transactional
    void deleteAllByUserId(Integer userId);
}
