package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * CommentDao.
 */
@Repository
public interface CommentDAO extends JpaRepository<Comment, Integer> {
    Page<Comment> findAllByResourceId(Integer id, Pageable pageable);

    List<Comment> findByResourceId(Integer id);

    @Transactional
    void deleteAllByResourceId(Integer id);
}
