package com.free4inno.knowledgems.service;

import com.free4inno.knowledgems.dao.ResourceDAO;
import com.free4inno.knowledgems.domain.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Author HUYUZHU.
 * Date 2021/1/30 23:11.
 */

@Slf4j
@Service
public class CleanResourceService {

    @Autowired
    private ResourceDAO resourceDao;

    @Autowired
    private ResourceEsService esService;
    /**
     * 删除一个群组，清除该群组下所有资源groupId中的该群组id.
     *
     * @param id 要删除的群组的id.
     */
    public void cleanGroupIdInResource(String id) {
        log.info(this.getClass().getName() + "----in----" + "清除该群组下所有资源中的该群组id" + "----");
        List<Resource> resourceList = resourceDao.findByGroupIdContaining(id);
        if (!resourceList.isEmpty()) {
            for (Resource resource : resourceList) {
                String outGroupIds = "";
                String[] groupIds = resource.getGroupId().split(",|，");
                for (String g : groupIds) {
                    if (!g.equals(id)) {
                        outGroupIds = outGroupIds + "," + g;
                    }
                }
                if (!outGroupIds.equals("")) {
                    outGroupIds = outGroupIds.substring(1);
                }
                resource.setGroupId(outGroupIds);
                resource.setEditTime(new Timestamp(new Date().getTime()));
                resourceDao.saveAndFlush(resource);
                esService.updateResourceES(resource.getId());
            }
        }
        log.info(this.getClass().getName() + "----out----" + "该群组下所有资源中的该群组id已清除" + "----");
    }

    /**
     * 删除一个二级标签，清除该二级标签下所有资源labelId中的该标签id.
     *
     * @param id 要删除的二级标签的id.
     */
    public void cleanLabelIdInResource(String id) {
        log.info(this.getClass().getName() + "----in----" + "清除该二级标签下所有资源中的该标签id" + "----");
        List<Resource> resourceList = resourceDao.findByLabelIdContaining(id);
        if (!resourceList.isEmpty()) {
            for (Resource resource : resourceList) {
                String outLabelIds = "";
                String[] labelIds = resource.getLabelId().split(",|，");
                for (String l : labelIds) {
                    if (!l.equals(id)) {
                        outLabelIds = outLabelIds + "," + l;
                    }
                }
                if (!outLabelIds.equals("")) {
                    outLabelIds = outLabelIds.substring(1);
                }
                resource.setLabelId(outLabelIds);
                resource.setEditTime(new Timestamp(new Date().getTime()));
                resourceDao.saveAndFlush(resource);
                esService.updateResourceES(resource.getId());
            }
        }
        log.info(this.getClass().getName() + "----out----" + "标签下所有资源中的该标签id已清除" + "----");
    }

    /**
     * 删除一个用户，将该用户所有资源转移至内容管理员.
     *
     * @param id 要删除的用户id.
     */
    public void cleanUserIdInResource(int id) {
        log.info(this.getClass().getName() + "----in----" + "将该用户所有资源转移至内容管理员" + "----");
        List<Resource> resourceList = resourceDao.findByUserId(id);
        if (!resourceList.isEmpty()) {
            for (Resource resource : resourceList) {
                resource.setUserId(40);
                resource.setUserName("内容管理员");
                resourceDao.saveAndFlush(resource);
                esService.updateResourceES(resource.getId());
            }
        }
        log.info(this.getClass().getName() + "----out----" + "该用户所有资源已经转移至内容管理员" + "----");
    }

}
