package com.free4inno.knowledgems.constants;

/**
 * 作者：LHJ
 */
public class LabelConstants {

    public static final String CODE = "code";
    public static final String MSG = "msg";
    public static final String RESULT = "result";
}
