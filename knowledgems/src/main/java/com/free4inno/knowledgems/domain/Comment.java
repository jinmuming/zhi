package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.sql.Timestamp;

/**
 * Comment.
 */
@Entity
@Table(name = "comment")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "resource_id")
    private Integer resourceId; //资源ID

    @Column(name = "user_id")
    private Integer userId; //评论用户ID

    @Column(name = "content", columnDefinition = "TEXT")
    private String content; //评论内容

    @Column(name = "time")
    private Timestamp time; //评论时间

    @Transient
    private String userName; //评论用户姓名

    @Transient
    private String picName; //自动生成头像的图片文件名
}
