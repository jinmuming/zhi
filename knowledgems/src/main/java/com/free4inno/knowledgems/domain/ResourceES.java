package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Id;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;

/**
 * ResourceES.
 */

@Document(indexName = "knowledge_ms_v3", type = "resource")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResourceES {
    @Id
    private Integer id;
    private Integer crop_id;
    private Integer user_id;
    private Timestamp create_time;
    private Integer resourceId;
    private Timestamp edit_time;
    @Field(type = FieldType.Auto, searchAnalyzer = "ik_smart", analyzer = "ik_max_word")
    private String title;
    @Field(type = FieldType.Auto, searchAnalyzer = "ik_smart", analyzer = "ik_max_word")
    private String text;
    private String attach_text;
    private ArrayList<Object> attachment;
    private Map<String, HighlightField> highlight;
    private Map<String, SearchHits> innerHitsContent;
    private Integer attach_search_flag;
    private Integer comment_search_flag;
    private Integer text_search_flag;
    private ArrayList<Object> comment;
    private Integer superior;
    private Integer recognition;
    private Integer opposition;
    private Integer pageview;
    private Integer collection;
    @Field(type = FieldType.Auto, analyzer = "douhao")
    private String group_id;
    @Field(type = FieldType.Auto, analyzer = "douhao")
    private String label_id;
    private String permissionId;
    @Transient
    private String user_name;
    @Transient
    private String group_name;
    @Transient
    private String label_name;

}
