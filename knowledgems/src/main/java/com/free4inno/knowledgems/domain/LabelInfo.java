package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;


/**
 * Author HaoYi.
 * Date 2020/9/11.
 */

@Entity
@Table(name = "label_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LabelInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "label_id")
    private Integer id; //主键

    @Column(name = "crop_id")
    private Integer cropId; //CropId

    @Column(name = "label_name")
    private String labelName; //标签名

    @Column(name = "label_info", columnDefinition = "TEXT")
    private String labelInfo; //用户组描述

    @Column(name = "create_time")
    private Timestamp createTime; //资源创建时间

    @Column(name = "edit_time")
    private Timestamp editTime; //资源修改时间

    @Column(name = "uplevel_id")
    private Integer uplevelId; //默认为0，即为一级标签，如果不为零则为对应label_id的下级标签

    public LabelInfo(int id) {
        this.id = id;
    }
}
