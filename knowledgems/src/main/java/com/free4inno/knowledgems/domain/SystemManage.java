package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.sql.Timestamp;

/**
 * Author: ZhaoHaotian.
 * Date: 2021/04/24.
 */
@Entity
@Table(name = "system_manage")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SystemManage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "variable")
    private String variable; // 变量名（主键）

    @Column(name = "value")
    private String value; // 变量值

    @Column(name = "set_time")
    private Timestamp setTime; // 修改时间

}
