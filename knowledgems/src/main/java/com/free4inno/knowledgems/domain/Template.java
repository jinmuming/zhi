package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.sql.Timestamp;

/**
 * Author: ZhaoHaotian.
 * Date: 2021/4/27.
 */

@Entity
@Table(name = "template")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Template {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;    // id（主键）

    @Column(name = "templateName", columnDefinition = "TEXT")
    private String templateName; //预留字段

    @Column(name = "templateCode", columnDefinition = "LONGTEXT")
    private String templateCode; //预留字段

    @Column(name = "createTime")
    private Timestamp createTime; //预留字段

    @Column(name = "setTime")
    private Timestamp setTime; //预留字段

}
