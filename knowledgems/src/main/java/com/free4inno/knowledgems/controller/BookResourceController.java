package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.dao.ResourceDAO;
import com.free4inno.knowledgems.domain.Resource;
import com.free4inno.knowledgems.service.ResourceService;
import com.free4inno.knowledgems.constants.UserConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: ZhaoHaotian.
 * Date: 2021/8/5.
 */

@Slf4j
@Controller
@RequestMapping("/bookResource")
public class BookResourceController {

    @Autowired
    private ResourceDAO resourceDao;

    @Autowired
    private ResourceService resourceService;


//    @RequestMapping("/bookResource")
//    public String bookResource(@RequestParam("id") int id, HttpSession session, Map param) {
//        log.info(this.getClass().getName()+"----in----"+"进入书籍(bookResource)"+"----"+session.getAttribute(Constants.USER_ID));
//
//        // get resource by id
//        Resource resource = resourceDao.findAllById(id);
//        // resource.setGroupName(resourceService.getGroupName(resource));
//        // resource.setLabelName(resourceService.getLabelName(resource));
//
//        // put param
//        param.put("resource", resource);
//
//        // check login
//        if (session.getAttribute(Constants.ACCOUNT) == null) {
//            log.info(this.getClass().getName()+"----out----"+"未登录，跳转登录页面"+"----"+session.getAttribute(Constants.USER_ID));
//            return "/login/login";
//        } else {
//            log.info(this.getClass().getName()+"----out----"+"跳转书籍详情页"+"----"+session.getAttribute(Constants.USER_ID));
//            return "/resource/bookResource";
//        }
//    }

    @ResponseBody
    @GetMapping("/linkResource")
    public Map<String, Object> linkResource(@RequestParam("id") int id, HttpSession session) {
        log.info(this.getClass().getName()+"----in----"+"获取链接资源(linkResource)"+"----"+session.getAttribute(UserConstants.USER_ID));

        // get resource
        Resource resource = resourceDao.findAllById(id);
        // get group
        // resource.setGroupName(resourceService.getGroupName(resource));
        // get label
        // resource.setLabelName(resourceService.getLabelName(resource));
//        // get attachment
//        String attachment = resourceService.getAttachmentJson(id);

        // build json
        Map<String, Object> jsonObject = new HashMap<>();
        jsonObject.put("resource", resource);
        jsonObject.put("attachment", "");

        log.info(this.getClass().getName()+"----out----"+"异步返回目录所链接到的资源内容"+"----"+session.getAttribute(UserConstants.USER_ID));

        return jsonObject;
    }

    @ResponseBody
    @GetMapping("/linkAttachment")
    public Map<String, Object> linkAttachment(@RequestParam("id") int id, HttpSession session) {
        log.info(this.getClass().getName()+"----in----"+"获取链接附件(linkAttachment)"+"----"+session.getAttribute(UserConstants.USER_ID));

        // get attachment
        String attachment = resourceService.getAttachmentJson(id);

        // build json
        Map<String, Object> jsonObject = new HashMap<>();
        jsonObject.put("attachment", attachment);

        log.info(this.getClass().getName()+"----out----"+"异步返回目录所链接到的附件内容"+"----"+session.getAttribute(UserConstants.USER_ID));

        return jsonObject;
    }
}
