package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.constants.GroupConstants;
import com.free4inno.knowledgems.dao.SignupInfoDAO;
import com.free4inno.knowledgems.dao.UserDAO;
import com.free4inno.knowledgems.domain.SignupInfo;
import com.free4inno.knowledgems.domain.User;
import com.free4inno.knowledgems.service.UserService;
import com.free4inno.knowledgems.constants.UserConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

/**
 * Author HUYUZHU.
 * Date 2020/10/15 11:02.
 */

@Slf4j
@Controller
@Transactional
public class LoginController {

    @Autowired
    private UserDAO userDao;

    @Autowired
    private SignupInfoDAO signupInfoDao;

    @Autowired
    private UserService userService;

    @GetMapping("login")
    public String login(HttpSession session) {
        //log.info(this.getClass().getName() + "----in----");
        if (session.getAttribute(UserConstants.ACCOUNT) == null) {
            //log.info(this.getClass().getName() + "----out----" + "跳转登录页面----" + "未登录");
            return "/login/login";
        } else {
            //log.info(this.getClass().getName() + "----out----" + "重定向到首页----" + session.getAttribute(Constants.USER_ID));
            return "redirect:/";
        }
    }

    @PostMapping("login")
    public String login(@RequestParam("account") String account,
                        @RequestParam("psw") String psw,
                        Map params,
                        HttpSession session) {
        log.info(this.getClass().getName() + "----in----");
        User user = new User();
        if (account.contains("@")) {
            //输入为邮箱
            user = userDao.findByMailAndUserPassword(account, psw);
        } else {
            //输入为手机号
            user = userDao.findByAccountAndUserPassword(account, psw);
        }
        if (user == null) {
            params.put("wrongOfEmailOrPsw", true);
            log.info(this.getClass().getName() + "----out----" + "校验失败，跳转登录页面----" + "未登录");
            return "/login/login";
        } else {
            params.put("wrongOfEmailOrPsw", false);
            session.setAttribute(UserConstants.ACCOUNT, account);
            session.setAttribute(UserConstants.ACCOUNT_NAME, user.getAccountName());
            session.setAttribute(UserConstants.ACCOUNT_MAIL, user.getMail());
            session.setAttribute(UserConstants.USER_ID, user.getId());
            session.setAttribute(UserConstants.USER_NAME, user.getRealName());
            session.setAttribute(GroupConstants.ROLE_ID, user.getRoleId());
            session.setAttribute("login_flag", 1);
            session.setMaxInactiveInterval(120 * 60);//设置session过期时间为120min
            String uri = (String) session.getAttribute(UserConstants.COMPLETE_URI);
            session.removeAttribute(UserConstants.COMPLETE_URI);
            log.info(this.getClass().getName()+"----"+"已登录用户----" + session.getAttribute(UserConstants.USER_ID));
            if (uri == null || uri.equals("") || uri.equals("/logout")) {
                log.info(this.getClass().getName() + "----out----" + "校验通过，重定向到首页----" + session.getAttribute(UserConstants.USER_ID));
                return "redirect:/";
            } else {
                log.info(this.getClass().getName() + "----out----" + "校验通过，重定向到"+uri+"----" + session.getAttribute(UserConstants.USER_ID));
                return "redirect:" + uri;
            }
        }
    }

    @GetMapping("logout")
    @PostMapping("logout")
    @RequestMapping("logout")
    public String logout(HttpSession session) {
        log.info(this.getClass().getName()+"----in----"+"用户登出(logout)"+"----"+session.getAttribute(UserConstants.USER_ID));
        session.removeAttribute(UserConstants.ACCOUNT);
        session.removeAttribute(UserConstants.ACCOUNT_NAME);
        session.removeAttribute(UserConstants.ACCOUNT_MAIL);
        session.removeAttribute(UserConstants.USER_ID);
        session.removeAttribute(UserConstants.USER_NAME);
        session.removeAttribute(GroupConstants.ROLE_ID);
        session.setAttribute("login_flag", 0);
        log.info(this.getClass().getName()+"----out----"+"用户已经登出，重定向到首页"+"----"+session.getAttribute(UserConstants.USER_ID));
        return "redirect:/";
    }

    @GetMapping("signup")
    public String signup() {
        log.info(this.getClass().getName()+"----in----"+"请求注册(signup)"+"----");
        log.info(this.getClass().getName()+"----out----"+"返回注册页面"+"----");
        return "/login/signup";
    }

    @PostMapping("signup")
    @ResponseBody
    public String signup(
            @RequestParam("name") String name,
            @RequestParam("mail") String mail,
            @RequestParam("telnumber") String telnumber,
            @RequestParam("reason") String reason) {
        log.info(this.getClass().getName()+"----in----"+"提交注册(signup)"+"----");
        if (userService.userExist(mail, telnumber)) {
            log.info(this.getClass().getName()+"----out----"+"注册请求信息提交失败(手机或邮箱重复)"+"----");
            return "false";
        } else {
            SignupInfo signupInfo = new SignupInfo();
            signupInfo.setName(name);
            signupInfo.setMail(mail);
            signupInfo.setTelnumber(telnumber);
            signupInfo.setReason(reason);
            signupInfo.setStatus(0);
            signupInfo.setSignupTime(new Timestamp(new Date().getTime()));
            signupInfoDao.saveAndFlush(signupInfo);
            log.info(this.getClass().getName()+"----out----"+"注册请求信息提交成功"+"----");
            return "success";
        }
    }

}
