package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.constants.UserConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.free4inno.knowledgems.utils.CompleteUriUtils.getCompleteUri;

/**
 * IndexController.
 */

@Slf4j
@Controller
@RequestMapping("")
public class IndexController {

    @RequestMapping("")
    public String root(HttpSession session, HttpServletRequest request) {
        log.info(this.getClass().getName()+"----in----" + "根路径处理(root)" + "----");
        String uri = getCompleteUri(request);
        session.setAttribute(UserConstants.COMPLETE_URI, uri);
        if (session.getAttribute("login_flag") == null || session.getAttribute("login_flag").equals(0)) {
            session.setAttribute("login_flag", 0);
            log.info(this.getClass().getName()+"----out----" + "跳转到homepage" + "----未登录");
            return "index/home_search";
        } else {
            log.info(this.getClass().getName()+"----out----" + "跳转到homepage" + "----已登录" + session.getAttribute(UserConstants.USER_ID));
            return "index/home_search_login";
        }
    }

}
