package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.constants.UserConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Author HUYUZHU.
 * Date 2021/2/6 14:08.
 */

@Slf4j
@Controller
@RequestMapping("error")
public class ErrorController {

    /**
     * 404页面.
     */
    @RequestMapping(value = "/404")
    public String error_404(HttpSession session) {
        log.info(this.getClass().getName()+"----in----"+"404"+"----"+session.getAttribute(UserConstants.USER_ID));
        log.info(this.getClass().getName()+"----out----"+"返回error_404页面"+"----"+session.getAttribute(UserConstants.USER_ID));
        return "error/error_404";
    }

    /**
     * 500页面.
     */
    @RequestMapping(value = "/500")
    public String error_500(HttpSession session) {
        log.info(this.getClass().getName()+"----in----"+"500"+"----"+session.getAttribute(UserConstants.USER_ID));
        log.info(this.getClass().getName()+"----out----"+"返回error_500页面"+"----"+session.getAttribute(UserConstants.USER_ID));
        return "error/error_500";
    }

}
