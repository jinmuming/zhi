package com.free4inno.knowledgems.config;

import com.alibaba.fastjson.JSONObject;
import com.free4inno.knowledgems.domain.Attachment;
import com.free4inno.knowledgems.domain.Resource;
import com.free4inno.knowledgems.domain.ResourceES;
import com.free4inno.knowledgems.service.ParseMailService;
import com.free4inno.knowledgems.service.ResourceEsService;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;

@Configuration
@EnableScheduling
@Slf4j
public class MailAutoCheckTask {

    @Autowired
    private ParseMailService parseMailService;

    @Scheduled(cron = "0 0/60 * * * ?")
    public void CheckMail() {
//        parseMailService.run();
        //23.7.5注释掉邮件自动更新 避免在知了中产生垃圾邮件：刘新元
    }
}
