package com.free4inno.knowledgems.config;

import com.free4inno.knowledgems.dao.CronDAO;
import com.free4inno.knowledgems.service.ResourceEsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;


/**
 * Author LIYUNZE.
 * Date 2021/3/19.
 */
@Configuration
@EnableScheduling
public class DynamicScheduleTask implements SchedulingConfigurer {

    @Autowired
    private CronDAO cronDao;

    @Autowired
    private ResourceEsService esService;

    /**
     * 执行定时任务.
     */
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
//        taskRegistrar.addTriggerTask(
//                /* 添加任务内容 */
//                () -> esService.updateResourceES(),
//                /* 设置执行周期 */
//                triggerContext -> {
//                    /* 从数据库获取执行周期 */
//                    String cron = cronDao.findByCronId(1).getCron();
//                    /* 返回执行周期 */
//                    return new CronTrigger(cron).nextExecutionTime(triggerContext);
//                }
//        );
    }
}
