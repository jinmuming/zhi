package com.free4inno.knowledgems.openapi;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Author HUYUZHU.
 * Date 2021/10/24 18:15.
 * OpenAPI 预置返回码及信息
 * 枚举类
 */

@Getter
@AllArgsConstructor
public enum ResultEnum {
    SUCCESS(200, "业务处理成功"),
    WORK_ERROR(202, "业务处理失败"),
    ACCESS_ERROR(401, "验签失败"),
    ARGS_ERROR(402, "验参失败"),
    FORBIDDEN(403, "权限不足");

    private Integer code;
    private String msg;

}
