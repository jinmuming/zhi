<%--
  Created by IntelliJ IDEA.
  User: killover
  Date: 2017/3/15
  Time: 10:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!-- Object Fit Images -->
<%--<script src="../assets/vendor/object-fit-images/dist/ofi.min.js"></script>--%>

<!-- Popper -->
<%--<script src="../assets/vendor/popper.js/dist/umd/popper.min.js"></script>--%>

<!-- Bootstrap -->
<script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<%--<script src="../assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>--%>

<!-- Fancybox -->
<!--<script src="../assets/vendor/fancybox/dist/jquery.fancybox.min.js"></script>-->

<!-- Cleave -->
<!--<script src="../assets/vendor/cleave.js/dist/cleave.min.js"></script>-->

<!-- Validator -->
<!--<script src="../assets/vendor/validator/validator.min.js"></script>-->

<!-- Sticky Kit -->
<!--<script src="../assets/vendor/sticky-kit/dist/sticky-kit.min.js"></script>-->

<!-- Jarallax -->
<!--<script src="../assets/vendor/jarallax/dist/jarallax.min.js"></script>-->
<!--<script src="../assets/vendor/jarallax/dist/jarallax-video.min.js"></script>-->

<!-- Isotope -->
<script src="../assets/vendor/isotope-layout/dist/isotope.pkgd.min.js"></script>

<!-- ImagesLoaded -->
<!--<script src="../assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>-->

<!-- Swiper -->
<!--<script src="../assets/vendor/swiper/dist/js/swiper.min.js"></script>-->

<!-- Gist Embed -->
<!--<script src="../assets/vendor/gist-embed/gist-embed.min.js"></script>-->

<!-- Bootstrap Select -->
<script src="../assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Dropzone -->
<!--<script src="../assets/vendor/dropzone/dist/min/dropzone.min.js"></script>-->

<!-- Quill -->
<!--<script src="../assets/vendor/quill/dist/quill.min.js"></script>-->

<!-- The Amdesk
<script src="../assets/js/amdesk.min.js"></script>
<script src="../assets/js/amdesk-init.js"></script>-->

<%--pagenum--%>
<script src="../assets/js/pagenum.js"></script>

<%--loading--%>
<script src="../assets/js/loading.js"></script>

<%--front--%>
<script src="../assets/vendor/newfront/front.js"></script>

<%--cookie--%>
<script src="../assets/js/jquery.cookie.js"></script>

<%--md5--%>
<script src="../assets/js/jquery.md5.js"></script>

<%--custom--%>
<script>
    //搜索条件展示,控制开关，num为1说明没有搜索到资源，为2则搜索到了资源
    function clickCriteria(id, num) {
        if (id == 1) {
            if (num == 1) {
                document.getElementById('search-criteria-text').style.display = "block";
                var htmlCri1 = "<a href=\"javascript:void(0);\" onclick=\"clickCriteria(2,1)\" >折叠搜索条件</a>";
                $("#search-criteria").html(htmlCri1);
            }
            if (num == 2) {
                document.getElementById('search-criteria-text1').style.display = "block";
                var htmlCri2 = "<a href=\"javascript:void(0);\" onclick=\"clickCriteria(2,2)\" >折叠搜索条件</a>";
                $("#search-criteria1").html(htmlCri2);
            }
        } else {
            if (num == 1) {
                document.getElementById('search-criteria-text').style.display = "none";
                var htmlCri3 = "<a href=\"javascript:void(0);\" onclick=\"clickCriteria(1,1)\" >展开搜索条件</a>";
                $("#search-criteria").html(htmlCri3);
            }
            if (num == 2) {
                document.getElementById('search-criteria-text1').style.display = "none";
                var htmlCri4 = "<a href=\"javascript:void(0);\" onclick=\"clickCriteria(1,2)\" >展开搜索条件</a>";
                $("#search-criteria1").html(htmlCri4);
            }
        }
    }

    //重新搜索，带着搜索条件跳转至高级搜索页面
    function searchAgain(queryStr, groupIds, labelIds) {
        var searchQueryStr = queryStr.trim();
        var searchGroupIds = groupIds.replace(/\s*/g,"");
        var searchLabelIds = labelIds.replace(/\s*/g,"");
        window.location.href = "<%=request.getContextPath()%>/adsearch/adsearch?searchQueryStr=" + searchQueryStr + "&searchGroupIds=" + searchGroupIds + "&searchLabelIds=" + searchLabelIds;
    }

    // 修改密码
    function confirmChangePassword(){
        const newPassword = $('#newPassword').val();
        const confirmPassword = $('#confirmPassword').val();
        if(newPassword == confirmPassword){
            changePassword();
        }else {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-ok-sign', content: '修改失败,新密码不一致'});
        }
    }

    function showChangePasswordModal(id){
        $('#changePasswordModal').modal();
        $('#userId').val(id);
    }

    function changePassword(){
        const newPassword = $('#newPassword').val();
        const oldPassword = $('#oldPassword').val();
        let oldPsd = $.md5(oldPassword);
        let psd = $.md5(newPassword);
        let changeId = $('#userId').val();
        const newUser = {
            psd: psd,
            oldPsd: oldPsd,
            changeId: changeId
        };
        $.ajax({
            type: "patch",
            url: "/user/changePassword",
            data: newUser,
            success: function (jsonObject) {
                if (jsonObject.code === 200){
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '修改成功,3秒后重新登录'});
                    setTimeout(afterChange,3000)
                }else{
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-ok-sign', content: '原密码有误，请重新输入'});
                    console.log(oldPsd)
                }
            },
        });
    }

    function afterChange(){
        window.location.href = "<%=request.getContextPath()%>/logout";
    }

    // 个人信息页面
    function showUserInfoModal(name, phone, mail, role){
        $('#userName').val(name);
        $('#userAccount').val(phone);
        $('#userMail').val(mail);
        $('#userInfo').modal();
        if(role == 0){
            $('#userRole').val("普通用户");
        }
        if(role == 1){
            $('#userRole').val("超级管理员");
        }
        if(role == 2){
            $('#userRole').val("用户管理员");
        }
        if(role == 3){
            $('#userRole').val("内容管理员");
        }
        if(role == 4){
            $('#userRole').val("系统管理员");
        }
    }

    //退出登录confirm
    function logout() {
        $.tipModal('confirm', 'warning', '确认退出吗？', function (result) {
            if (result == true) {
                window.location.href = "<%=request.getContextPath()%>/logout";
            }
        });
    }
</script>

