<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/9/4
  Time: 15:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>${templateInfo.templateName}-模板--知了[团队知识管理应用]</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
</head>

<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp"/>
    <button class="box_top hover_box" style="z-index: 2" onclick="goToBottom()">
        <img src="../assets/images/Down.png" height="20" width="20" />
    </button>
    <div class="re-box-6 mt-85">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="re-box re-box-decorated resource-post">
                        <div class="resource-post-box">
                            <ul class="re-breadcrumbs pb-25" id="breadCrumb">
                                <a href="../manage">管理</a>&nbsp;|&nbsp;<a href="javascript:void(0)" onclick="javascript :history.back(-1);">模板管理</a>&nbsp;|&nbsp;模板详情
                            </ul>
                            <%-------------------- 资源标题 --------------------%>
                            <h1 class="h3 resource-post-title">${templateInfo.templateName}</h1>

                            <%-------------------- 资源时间 --------------------%>
                            <ul class="resource-post-info">
                                <li class="font-text-darkgray">时间:&nbsp;</li>
                                <li class="font-text-darkgray"><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
                                                                               value="${templateInfo.createTime}"/></li>
                            </ul>
                            <%-------------------- 资源操作 --------------------%>
                            <div id="operation">
                                  <button class="re-btn re-btn-unselected re-label-child" onclick="window.open('<%=request.getContextPath()%>/template/editTemplate?id=${templateInfo.id}','_self');">编辑
                                    </button>
                                    <button class="re-btn re-btn-unselected re-label-child" onclick="deleteTemplateContent(${templateInfo.id})">
                                        删除
                                    </button>
<%--                                <button class="re-btn re-btn-unselected re-label-child" onclick="window.open('<%=request.getContextPath()%>/template/templateInfo','_self');">返回模板列表--%>
                                </button>

                            </div>
                        </div>
                        <%-------------------- 资源正文 --------------------%>
                        <div class="resource-post-box resource-post-article">
                            ${templateInfo.templateCode}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="box_bottom hover_box" onclick="goToTop()">
        <img src="../assets/images/Up.png" height="20" width="20" />
    </button>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>


</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>

<script language="JavaScript">
    function goToTop() {
        window.scrollTo(0, 0);
    }
    function goToBottom() {
        console.log(111)
        window.scrollTo(0, document.documentElement.scrollHeight-document.documentElement.clientHeight);
    }

    <%--userId = ${user_id};--%>

    let _commentTotal = 0;
    let _thisPageTotal = 0;

    var id = ${templateInfo.id};

    <%--function edit() {--%>
    <%--    alert(id)--%>
    <%--    window.location.href = "<%=request.getContextPath()%>/template/editTemplate?id=" + id;--%>
    <%--}--%>

    $(document).ready(function () {
        getComment(1);
    })



    //删除资源
    function deleteTemplateContent(templateId) {
        $.tipModal('confirm', 'warning', '确认删除模板吗?', function (result) {
            if (result == true) {
                $.ajax({
                    url:"/template/deleteTemplate",
                    type:"post",
                    async: false,
                    dataType:"text",
                    data:{templateId:templateId},
                    success:function(data) {
                        if(data=="ok") {
                            $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '删除模板成功'});
                            <%--getMember(${groupId});--%>
                        }else{
                            // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '设置管理员失败'});
                        }
                    },
                });
                    <%--$.post("${pageContext.request.contextPath}/util/deleteAttachment", {--%>
                    <%--    attachmentId: attachmentId,--%>
                    <%--    saveUrl: saveUrl--%>
                    <%--});--%>
                window.location.href = "<%=request.getContextPath()%>/template/templateInfo";
            }
        });
    }


    var time2;


    document.addEventListener("keydown", function (e) {
        if (e.keyCode == 13 && e.ctrlKey) {
            console.log("按下ctrl同时回车")
            $("#submitCommentButton").click();
        }
    });

    document.addEventListener("keydown", function (e) {
        if (e.keyCode == 13 && e.ctrlKey) {
            console.log("按下ctrl同时回车")
            $("#submitCommentButton").click();
        }
    });


</script>

<style>
    #mytitle {
        position: absolute;
        color: #ffffff;
        max-width: 160px;
        font-size: 14px;
        padding: 4px;
        background: rgba(40, 40, 40, 0.8);
        border: solid 1px #e9f7f6;
        border-radius: 5px;
        z-index: 999;
    }
     .re-btn{
         border-radius: 3px;
     }
</style>

<!-- END: Scripts -->
</html>