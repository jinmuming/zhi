<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:choose>
    <c:when test="${roleID == 1 || roleID == 2}">
        <c:if test="${groupId == 0}">
            <c:forEach var="h" items="${memberList}" varStatus="status">
                <div class="btn-group dropup">
                    <button type="button" class="btn btn-secondary btn-outline-light dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" style="background-color: whitesmoke;color: black">
                            ${h.realName}
                        <c:if test="${h.permission == 1}">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trophy-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                            </svg>
                        </c:if>
                        <c:if test="${h.permission == 2}">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                            </svg>
                        </c:if>
                    </button>
                    <div class="dropdown-menu" style="position:relative; z-index: 99999">
                        <button class="dropdown-item"><a href="/myresource?userId=${h.userId}" style="color: black">查看个人资料</a></button>
                        <c:if test="${h.permission == 0}">
                            <button class="dropdown-item" onclick=removeMemberfromdefault(${h.userId},${h.account})>移出内部用户群组</button>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </c:if>

        <c:if test="${groupId == 1}">
            <c:forEach var="h" items="${memberList}" varStatus="status">
                <div class="btn-group dropup">
                    <button type="button" class="btn btn-secondary btn-outline-light dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" style="background-color: whitesmoke;color: black">
                            ${h.realName}
                        <c:if test="${h.permission == 1}">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trophy-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                            </svg>
                        </c:if>
                        <c:if test="${h.permission == 2}">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                            </svg>
                        </c:if>
                    </button>
                    <div class="dropdown-menu" style="position:relative; z-index: 99999">
                        <button class="dropdown-item"><a href="/myresource?userId=${h.userId}" style="color: black">查看个人资料</a></button>
                    </div>
                </div>
            </c:forEach>
        </c:if>

        <c:if test="${groupId != 0 && groupId != 1}">
            <c:forEach var="h" items="${memberList}" varStatus="status">
                <div class="btn-group dropup">
                    <button type="button" class="btn btn-secondary btn-outline-light dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" style="background-color: whitesmoke;color: black">
                            ${h.realName}
                        <c:if test="${h.permission == 1}">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trophy-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                            </svg>
                        </c:if>
                        <c:if test="${h.permission == 2}">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                            </svg>
                        </c:if>
                    </button>
                    <div class="dropdown-menu" style="position:relative; z-index: 99999">
                        <button class="dropdown-item"><a href="/myresource?userId=${h.userId}" style="color: black">查看个人资料${h.account}</a></button>
                        <c:if test="${h.permission == 0}">
                            <button class="dropdown-item" onclick=setLeader(${groupId},${h.userId})>设为群主</button>
                            <button class="dropdown-item" onclick=setAdmin(${groupId},${h.userId})>设为群组管理员</button>
                            <button class="dropdown-item" onclick=deleteMember(${groupId},${h.userId})>从群组中删除</button>
                        </c:if>
                        <c:if test="${h.permission == 2}">
                            <button class="dropdown-item" onclick=setLeader(${groupId},${h.userId})>设为群主</button>
                            <button class="dropdown-item" onclick=cancelAdmin(${groupId},${h.userId})>撤销群组管理员</button>
                            <button class="dropdown-item" onclick=deleteMember(${groupId},${h.userId})>从群组中删除</button>
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </c:if>

    </c:when>
    <c:when test="${userPermission == 1 && roleID != 1 && roleID != 2}">
        <c:forEach var="h" items="${memberList}" varStatus="status">
            <div class="btn-group dropright">
                <button type="button" class="btn btn-secondary btn-outline-light dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" style="background-color: whitesmoke;color: black">
                        ${h.realName}
                    <c:if test="${h.permission == 1}">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trophy-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                        </svg>
                    </c:if>
                    <c:if test="${h.permission == 2}">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                        </svg>
                    </c:if>
                </button>
                <div class="dropdown-menu" style="position:relative; z-index: 99999">
                    <button class="dropdown-item"><a href="/myresource?userId=${h.userId}" style="color: black">查看个人资料</a></button>
                    <c:if test="${h.permission == 0}">
                        <button class="dropdown-item" onclick=setAdmin(${groupId},${h.userId})>设为群组管理员</button>
                        <button class="dropdown-item" onclick=deleteMember(${groupId},${h.userId})>从群组中删除</button>
                    </c:if>
                    <c:if test="${h.permission == 2}">
                        <button class="dropdown-item" onclick=cancelAdmin(${groupId},${h.userId})>撤销群组管理员</button>
                        <button class="dropdown-item" onclick=deleteMember(${groupId},${h.userId})>从群组中删除</button>
                    </c:if>
                </div>
            </div>
        </c:forEach>
    </c:when>
    <c:when test="${userPermission == 2}">
        <c:forEach var="h" items="${memberList}" varStatus="status">
            <div class="btn-group dropright">
                <button type="button" class="btn btn-secondary btn-outline-light dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" style="background-color: whitesmoke;color: black">
                        ${h.realName}
                    <c:if test="${h.permission == 1}">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trophy-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                        </svg>
                    </c:if>
                    <c:if test="${h.permission == 2}">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                        </svg>
                    </c:if>
                </button>
                <div class="dropdown-menu">
                    <button class="dropdown-item"><a href="/myresource?userId=${h.userId}" style="color: black">查看个人资料</a></button>
                    <c:if test="${h.permission == 0}">
                        <button class="dropdown-item" onclick=deleteMember(${groupId},${h.userId})>从群组中删除</button>
                    </c:if>
                </div>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
        <c:forEach var="h" items="${memberList}" varStatus="status">
            <%--                                            <br>--%>
            <%--                                            <div>--%>
            <div class="btn-group dropright">
                <button type="button" class="btn btn-secondary btn-outline-light dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" style="background-color: whitesmoke;color: black">
                        ${h.realName}
                    <c:if test="${h.permission == 1}">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trophy-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z"/>
                        </svg>
                    </c:if>
                    <c:if test="${h.permission == 2}">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                        </svg>
                    </c:if>
                </button>
                <div class="dropdown-menu">
                    <button class="dropdown-item"><a href="/myresource?userId=${h.userId}" style="color: black">查看个人资料</a></button>
                </div>
            </div>
        </c:forEach>
    </c:otherwise>
</c:choose>

<script>
    function removeMemberfromdefault(userId, userAccount){
        $.ajax({
            url:"/deleteMember",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:0,
                userId:userId
            },
            success:function(data) {
                if(data=="ok") {
                    addMembertoout(userAccount);
                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '删除成员失败'});
                }
            },
        });
    }


    function addMembertoout(userAccount){
        $.ajax({
            url:"/addMember",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:1,
                userAccount:userAccount
            },
            success:function(data) {
                if(data=="ok") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '用户已被移入外部访客群组'});
                    getMember(${groupId});
                    createSelectPicker();
                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '删除成员失败'});
                }
            },
        });
    }

</script>