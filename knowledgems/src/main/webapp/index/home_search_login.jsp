<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2021/1/29
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>首页-知了[团队知识管理应用]</title>
</head>

<body onload="focus_init()">
<div class="re-main">
    <c:import url="../template/_navbar.jsp?menu=index"/>
    <header class="re-header re-box-1">
        <div class="container">
            <div class="home-image home-image-parallax">
                <img src="../assets/images/bg-header-2.png" class="jarallax-img" alt="">
                <div style="background-color: rgba(27, 27, 27, .8);"></div>
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                    <h1 class="mb-30 text-white text-center">团队知识管理</h1>
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <form id="search-form" class="re-form re-form-group-inputs">
                                <%--输入框--%>
                                <input type="text" style="display: none;"/>
                                <input type="hidden" name="keyword" id="hkeyword"/>
                                <input id="query" type="text" name="queryStr" class="form-control re-btn-rd" placeholder="在这里发现你想要的知识"/>
                                <button type="button" class="re-btn re-btn-lg re-btn-r" onclick="searchData(1)"><a>搜&nbsp;&nbsp;索</a></button>
                            </form>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                        <%--                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"--%>
                        <%--                             style="display: flex;/*实现垂直居中*/align-items: center;padding-left: 0px;">--%>
                        <%--                            <a href="../adsearch/adsearch" style="color: #ffffff">高级</a>--%>
                        <%--                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div>
        <div class="re-box bg-white">
            <div class="container">
                <ul class="home-navbar text-center">
                    <li class="active" id="recommendLabel" onclick="getRecommendResourcePage(1)">推荐</li>
                    <li id="newestLabel" onclick="getNewestResourcePage(1)">最新</li>
                    <li id="noticeLabel" onclick="getNoticeResourcePage(1)">公告</li>
                    <li id="myLabel" onclick="getMyResourcePage(1)">我的</li>
                </ul>
            </div>
        </div>
        <div class="re-separator"></div>
        <div class="re-box-6 re-grey">
            <div class="container">
                <div class="row vertical-gap md-gap">
                    <div class="col-lg-12">
                        <div class="re-box re-box-decorated">
                            <div id="search-data" class="resource-post">
                                <div class="resource-post-box pt-30 pb-25">
                                    <div class="front-loading">
                                        <img src="../assets/images/loading.gif"/>
                                    </div>
                                    <div class="panel-body text-center">正在加载请稍候</div>
                                </div>
                            </div>
                            <div id="page-data" class="text-center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <c:import url="../template/_footer.jsp"/>
</div>

<div class="modal fade" id="crudRecommendOrder" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">修改推荐资源排序</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>资源推荐排序</label>
                    <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="recommendOrder" placeholder="请填写一个整数" oninput="value=value.replace(/[^\-?\d]/g,'')"/>
                    <input type="hidden" id="reco_resource_id">
                </div>
            </div>
            <div class="modal-footer">
                <%--                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>关闭</button>--%>
                <button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"
                        onclick="recommendResource()"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span><a>确定</a></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="crudNoticeOrder" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">修改公告资源排序</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>资源推荐排序</label>
                    <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="noticeOrder" placeholder="请填写一个整数" oninput="value=value.replace(/[^\-?\d]/g,'')"/>
                    <input type="hidden" id="noti_resource_id">
                </div>
            </div>
            <div class="modal-footer">
                <%--                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>关闭</button>--%>
                <button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"
                        onclick="noticeResource()"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span><a>确定</a></button>
            </div>
        </div>
    </div>
</div>


</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>

<script type="text/javascript">
    function focus_init(){
        //alert("warning");
        // 光标居中
        setTimeout(function () {
            $("#query").focus();
        },500)
    }

    var userId = '${sessionScope.userId}';
    var userName = '${sessionScope.userName}';
    var roleId = '${sessionScope.roleId}';
    //加载动画的html
    var loadinghtml = "<div class=\"resource-post pt-30 pb-25\">" +
        "<div class=\"front-loading\">" +
        "<img src=\"../assets/images/loading.gif\"/>" +
        "</div>" +
        "<div class=\"panel-body text-center\">正在加载请稍候</div>" +
        "</div>"

    //异步获取该用户"我的"资源列表
    function getMyResourcePage(page) {
        $("#recommendLabel").css("color", "#1b1b1b");
        $("#newestLabel").css("color", "#1b1b1b");
        $("#noticeLabel").css("color", "#1b1b1b");
        $("#myLabel").css("color", "#2b72e6");
        $("#page-data").html("");//清除页码，必要！！
        $("#search-data").html(loadinghtml);
        $.post("../myresource/queryMyResourceData", {
            userId: userId,
            userName: userName,
            page: page - 1
        }, function (data) {
            var parser = new DOMParser();
            var htmlDoc = parser.parseFromString("" + data, "text/html");
            var pageSize = parseInt(htmlDoc.getElementById("pageSize").value);
            appNum = parseInt(htmlDoc.getElementById("resourceNum").value);
            $("#search-data").html(data);
            //首页显示，把资源数量一行隐藏掉,放在”我的“标签后面
            if ($("#resourceNum").val() != 0) {
                $("#firstrows").hide();
            }
            // $("#myLabel").text("我的(" + $("#resourceNum").val() + ")");
            //页数大于一，分页
            if (pageSize > 1) {
                $("#page-data").html(getDivPageNumHtml(page, pageSize, "getMyResourcePage") + "<br /><br /><br /><br />");
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    //异步获取"最新"资源列表
    function getNewestResourcePage(page) {
        $("#recommendLabel").css("color", "#1b1b1b");
        $("#newestLabel").css("color", "#2b72e6");
        $("#noticeLabel").css("color", "#1b1b1b");
        $("#myLabel").css("color", "#1b1b1b");
        $("#page-data").html("");//清除页码，必要！！
        $("#search-data").html(loadinghtml);
        $.post("../search/searchdata", {
            queryStr: "",
            page: page - 1,
            groupIds: "",
            labelIds: "",
            searchTimes: 1
        }, function (data) {
            var parser = new DOMParser();
            var htmlDoc = parser.parseFromString("" + data, "text/html");
            var pageSize = parseInt(htmlDoc.getElementById("pageSize").value);
            appNum = parseInt(htmlDoc.getElementById("appNum").value);
            $("#search-data").html(data);
            //首页显示，把资源数量一行隐藏掉
            if ($("#appNum").val() != 0) {
                $("#firstrows").hide();
                $("#search-criteria-text1").hide();
            }else {
                $("#search-criteria-text").hide();
            }
            //页数大于一，分页
            if (pageSize > 1) {
                $("#page-data").html(getDivPageNumHtml(page, pageSize, "getNewestResourcePage") + "<br /><br /><br /><br />");
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    //异步获取推荐资源列表
    function getRecommendResourcePage(page) {
        $("#recommendLabel").css("color", "#2b72e6");
        $("#newestLabel").css("color", "#1b1b1b");
        $("#noticeLabel").css("color", "#1b1b1b");
        $("#myLabel").css("color", "#1b1b1b");
        $("#page-data").html("");//清除页码，必要！！
        $("#search-data").html(loadinghtml);
        $.post("../specresource", {
            type: 1,
            page: page - 1
        }, function (data) {
            var parser = new DOMParser();
            var htmlDoc = parser.parseFromString("" + data, "text/html");
            var pageSize = parseInt(htmlDoc.getElementById("pageSize").value);
            appNum = parseInt(htmlDoc.getElementById("resourceNum").value);
            $("#search-data").html(data);
            //首页显示，把资源数量一行隐藏掉
            if ($("#resourceNum").val() != 0) {
                $("#firstrows").hide();
                $("#search-criteria-text1").hide();
            }else {
                $("#search-criteria-text").hide();
            }
            //页数大于一，分页
            if (pageSize > 1) {
                $("#page-data").html(getDivPageNumHtml(page, pageSize, "getRecommendResourcePage") + "<br /><br /><br /><br />");
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    //异步获取公告资源列表
    function getNoticeResourcePage(page) {
        $("#recommendLabel").css("color", "#1b1b1b");
        $("#newestLabel").css("color", "#1b1b1b");
        $("#noticeLabel").css("color", "#2b72e6");
        $("#myLabel").css("color", "#1b1b1b");
        $("#page-data").html("");//清除页码，必要！！
        $("#search-data").html(loadinghtml);
        $.post("../specresource", {
            type: 2,
            page: page - 1
        }, function (data) {
            var parser = new DOMParser();
            var htmlDoc = parser.parseFromString("" + data, "text/html");
            var pageSize = parseInt(htmlDoc.getElementById("pageSize").value);
            appNum = parseInt(htmlDoc.getElementById("resourceNum").value);
            $("#search-data").html(data);
            //首页显示，把资源数量一行隐藏掉
            if ($("#resourceNum").val() != 0) {
                $("#firstrows").hide();
                $("#search-criteria-text1").hide();
            }else {
                $("#search-criteria-text").hide();
            }
            //页数大于一，分页
            if (pageSize > 1) {
                $("#page-data").html(getDivPageNumHtml(page, pageSize, "getNoticeResourcePage") + "<br /><br /><br /><br />");
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    //移除特殊资源
    function deleteSpecialResource(type, id){
        if(roleId==1||roleId==3){
            $.tipModal('confirm','warning','确认要将资源移除出推荐么?',function (result){
                console.log(type)
                console.log(id)
                if(result==true){
                    $.post("../specresource/deleteSpec",{
                        type : type,
                        resourceId : id
                    },function (data){
                        $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '移除成功'});
                        if(type==1){
                            getRecommendResourcePage(1);
                        }else if(type==2){
                            getNoticeResourcePage(1);
                        }else {
                            getMyResourcePage(1);
                        }
                    });
                }
            });
        }else {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '您没有删除权限'});
        }
    }

    //设置为推荐资源按钮跳转
    function showRecommendOrder(id){
        if(roleId==1||roleId==3){
            $('#crudRecommendOrder').modal();
            document.getElementById("reco_resource_id").setAttribute("value",id);
        }else {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '您没有编辑权限'});
        }
    }
    //设置为公告资源按钮跳转
    function showNoticeOrder(id){
        if(roleId==1||roleId==3){
            $('#crudNoticeOrder').modal();
            document.getElementById("noti_resource_id").setAttribute("value",id);
        }else {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '您没有编辑权限'});
        }
    }
    //设置资源为推荐（ 弹窗 确定按钮调用）
    function recommendResource(){
        var recommendOrder = parseInt(document.getElementById("recommendOrder").value);
        if(isNaN(recommendOrder) || typeof recommendOrder != "number"){
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '请填写一个正/负整数'});
            return false
        }else {
            loading("show");
            var reco_resource_id = parseInt(document.getElementById("reco_resource_id").value);
            console.log(recommendOrder)
            $.post("../specresource/crudSpec",{
                type : 1,
                resourceId : reco_resource_id,
                order : recommendOrder
            }, function (data){
                if(data.msg=="SUCCESS"){
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '修改推荐顺序成功'});
                    loading("reset");
                    //重新点击推荐按钮
                    document.getElementById("recommendLabel").onclick();
                }else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '出问题了，重新试试！'});
                    loading("reset");
                }
            })

        }
    }
    //设置资源为公告（ 弹窗 确定按钮调用）
    function noticeResource(){
        var noticeOrder = parseInt(document.getElementById("noticeOrder").value)
        if(isNaN(noticeOrder) || typeof noticeOrder != "number"){
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '请填写一个正/负整数'});
            return false
        }else {
            loading("show");
            var noti_resource_id = parseInt(document.getElementById("noti_resource_id").value);
            console.log(noticeOrder)
            $.post("../specresource/crudSpec",{
                type : 2,
                resourceId : noti_resource_id,
                order : noticeOrder
            }, function (data){
                if(data.msg=="SUCCESS"){
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '修改公告顺序成功'});
                    loading("reset");
                    //重新点击推荐按钮
                    document.getElementById("noticeLabel").onclick();
                }else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '出问题了，重新试试！'});
                    loading("reset");
                }

            })
        }
    }

    //按键搜索和回车搜索
    function searchData() {
        var times = 1;
        // times = searchTimes+1;
        <%
            session.setAttribute("queryStr", request.getParameter("queryStr"));
        %>
        var query = $("#query").val().trim();
        window.location.href = "<%=request.getContextPath()%>/search/searchAll?queryStr=" + query + "&groupIds=" + "&labelIds=" + "&times=" + times;
    }

    $("#query").keydown(function (event) {
        if (event.keyCode == 13) {
            <%
                session.setAttribute("queryStr", request.getParameter("queryStr"));
                //System.out.println("query: "+request.getParameter("queryStr"));
            %>
            var query = $("#query").val().trim();
            var times = 1;
            window.location.href = "<%=request.getContextPath()%>/search/searchAll?queryStr=" + query + "&groupIds=" + "&labelIds=" + "&times=" + times;
            searchTimes = times;
        }
    });

    //默认加载页面时刷新“推荐”的内容
    $(document).ready(function () {
        getRecommendResourcePage(1);
    });

</script>
<style>
    .re-btn{
        border-radius: 3px;
    }
</style>
<!-- END: Scripts -->
</html>
