<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2021/1/3
  Time: 19:44
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${resourceNum==0}">
    <div class="resource-post-box pt-30 pb-25 h6 mb-25">
        <span class="font-title-2"><strong>${userName}</strong></span>
        <span style="margin-right:5px;color: black" class="font-text"><strong>还没有发布资源 | <a href="mailto:${userEmail}" style="color: black">[${userEmail}]</a></strong></span>
    </div>
</c:if>
<c:choose>
    <c:when test="${resourceList.size()!=0}">
        <div class="resource-post-box pt-30 pb-25" id="firstrows">
            <span class="font-1"><strong>${userName}</strong></span>
            <span style="margin-right:5px;color: black" class="font-text"><strong>共发布 ${resourceNum} 个资源 | <a href="mailto:${userEmail}" style="color: black">[${userEmail}]</a></strong></span>
        </div>
        <div class="re-resource-table re-resource-default">
            <table class="re-resource-table re-resource-table-default">
                <tbody id="result-column">
                <c:forEach var="resource" items="${resourceList}">
                    <tr>
                        <c:if test="${resource.text == null}">
                            <th scope="row" style="padding-right: 50px" class="re-resource-table-topics">
                                <c:if test="${resource.searchedByAttachment == 0 and resource.searchedByComment == 0}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${resource.id}" target="_blank">${resource.title}</a></span>
                                </c:if>
                                <c:if test="${resource.searchedByAttachment == 1 and resource.searchedByComment == 0}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${resource.id}" target="_blank">${resource.title}</a>&nbsp;&nbsp;<span class="iconfont icon-fujian"></span></span>
                                </c:if>
                                <c:if test="${resource.searchedByAttachment == 0 and resource.searchedByComment == 1}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${resource.id}" target="_blank">${resource.title}</a>&nbsp;&nbsp;<span class="iconfont icon-pinglunxiao"></span></span>
                                </c:if>
                                <c:if test="${resource.searchedByAttachment == 1 and resource.searchedByComment == 1}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${resource.id}" target="_blank">${resource.title}</a>&nbsp;&nbsp;<span class="iconfont icon-fujian"></span>&nbsp;&nbsp;<span class="iconfont icon-pinglunxiao"></span></span>
                                </c:if>
                                <c:if test="${sessionScope.roleId==1 or sessionScope.roleId==3}">
                                    <c:if test="${resource.specType==1}">
                                        <span class="font-text-darkgray"><a class="link-darkgray" href="javascript:void(0);" onclick="deleteSpecialResource(${resource.specType}, ${resource.id})">[移除]</a>&nbsp;&nbsp;&nbsp;</span>
                                        <span class="font-text-darkgray"><a class="link-darkgray" href="javascript:void(0);" onclick="showRecommendOrder(${resource.id})">[排序 ${resource.specOrder}]</a>&nbsp;</span>
                                    </c:if>
                                    <c:if test="${resource.specType==2}">
                                        <span class="font-text-darkgray"><a class="link-darkgray" href="javascript:void(0);" onclick="deleteSpecialResource(${resource.specType}, ${resource.id})">[移除]</a>&nbsp;&nbsp;&nbsp;</span>
                                        <span class="font-text-darkgray"><a class="link-darkgray" href="javascript:void(0);" onclick="showNoticeOrder(${resource.id})">[排序 ${resource.specOrder}]</a>&nbsp;</span>
                                    </c:if>
                                </c:if>
                                <c:if test="${!resource.labelName.equals(\"该资源无标签\")}">
                                    <span style="float:right">
                                        <c:forEach var="label" items="${resource.labelName.split(',|，')}" varStatus="s">
                                            <c:forEach var="labelId" items="${resource.labelId.split(',|，')}" varStatus="gi">
                                                <c:if test="${s.index==gi.index}">
                                                    <c:choose>
                                                        <c:when test="${s.last}">
                                                            <span class="font-text-darkgray"><a class="link-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>&nbsp;</span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="font-text-darkgray"><a class="link-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>&nbsp;&nbsp;/&nbsp;&nbsp;</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </c:forEach>
                                        </c:forEach>
                                    </span>
                                </c:if>
                                <br>
                                <span class="font-text-darkgray"><a href="/myresource?userId=${resource.userId}" style="color: darkgray">${resource.userName}</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="font-text-darkgray">
                                    <fmt:formatDate value="${resource.createTime}" pattern="yyyy-MM-dd HH:mm"/>
                                </span>
                                <c:if test="${!resource.groupName.equals(\"该资源无群组\")}">
                                    <span style="float:right">
                                        <c:forEach var="groupName" items="${resource.groupName.split(',|，')}" varStatus="gn">
                                            <c:forEach var="groupId" items="${resource.groupId.split(',|，')}" varStatus="gi">
                                                <c:if test="${gn.index==gi.index}">
                                                    <c:choose>
                                                        <c:when test="${gn.last}">
                                                            <c:if test="${groupId.trim() != 0 && groupId.trim() != 1}">
                                                                <span class="font-text-darkgray"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>&nbsp;</span>
                                                            </c:if>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="font-text-darkgray"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>&nbsp;&nbsp;/&nbsp;&nbsp;</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </c:forEach>
                                        </c:forEach>
                                    </span>
                                </c:if>
                            </th>
                        </c:if>
                        <c:if test="${resource.text != null}">
                            <th scope="row" style="padding-right: 50px" class="re-resource-table-topics">
                                <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${resource.id}" target="_blank">${resource.title}</a></span>
                                <c:if test="${sessionScope.roleId==1 or sessionScope.roleId==3}">
                                    <c:if test="${resource.specType==1}">
                                        <span class="font-text-darkgray"><a class="link-darkgray" href="javascript:void(0);" onclick="deleteSpecialResource(${resource.specType}, ${resource.id})">[移除]</a>&nbsp;&nbsp;&nbsp;</span>
                                        <span class="font-text-darkgray"><a class="link-darkgray" href="javascript:void(0);" onclick="showRecommendOrder(${resource.id})">[排序 ${resource.specOrder}]</a>&nbsp;</span>
                                    </c:if>
                                    <c:if test="${resource.specType==2}">
                                        <span class="font-text-darkgray"><a class="link-darkgray" href="javascript:void(0);" onclick="deleteSpecialResource(${resource.specType}, ${resource.id})">[移除]</a>&nbsp;&nbsp;&nbsp;</span>
                                        <span class="font-text-darkgray"><a class="link-darkgray" href="javascript:void(0);" onclick="showNoticeOrder(${resource.id})">[排序 ${resource.specOrder}]</a>&nbsp;</span>
                                    </c:if>
                                </c:if>
                                <c:if test="${!resource.labelName.equals(\"该资源无标签\")}">
                                    <span style="float:right">
                                        <c:forEach var="label" items="${resource.labelName.split(',|，')}" varStatus="s">
                                            <c:forEach var="labelId" items="${resource.labelId.split(',|，')}" varStatus="gi">
                                                <c:if test="${s.index==gi.index}">
                                                    <c:choose>
                                                        <c:when test="${s.last}">
                                                            <span class="font-text-darkgray"><a class="link-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>&nbsp;</span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="font-text-darkgray"><a class="link-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>&nbsp;&nbsp;/&nbsp;&nbsp;</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </c:forEach>
                                        </c:forEach>
                                    </span>
                                </c:if>
                                <br>
                                <c:if test="${resource.text.length() > 150}">
                                    <span class="font-text"
                                          style="word-break: break-all">${resource.text.substring(0,150)}……</span>
                                </c:if>
                                <c:if test="${resource.text.length() <= 150}">
                                    <span class="font-text" style="word-break: break-all">${resource.text}</span>
                                </c:if>
                                <c:if test="${resource.text != null || resource.userId != null}">
                                    <br>
                                </c:if>
                                <span class="font-text"><a href="/myresource?userId=${resource.userId}" style="color: darkgray">${resource.userName}</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="font-text" style="color: darkgray">
                                    <fmt:formatDate value="${resource.createTime}" pattern="yyyy-MM-dd HH:mm"/>
                                </span>
                                <c:if test="${!resource.groupName.equals(\"该资源无群组\")}">
                                    <span style="float:right">
                                        <c:forEach var="groupName" items="${resource.groupName.split(',|，')}" varStatus="gn">
                                            <c:forEach var="groupId" items="${resource.groupId.split(',|，')}" varStatus="gi">
                                                <c:if test="${gn.index==gi.index}">
                                                    <c:choose>
                                                        <c:when test="${gn.last}">
                                                            <c:if test="${groupId.trim() != 0 && groupId.trim() != 1}">
                                                                <span class="font-text-darkgray"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>&nbsp;</span>
                                                            </c:if>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="font-text-darkgray"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>&nbsp;&nbsp;/&nbsp;&nbsp;</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </c:forEach>
                                        </c:forEach>
                                    </span>
                                </c:if>
                            </th>
                        </c:if>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </c:when>
</c:choose>

<input id="pageSize" type="hidden" value="${pageSize}"/>
<input id="resourceNum" type="hidden" value="${resourceNum}"/>
