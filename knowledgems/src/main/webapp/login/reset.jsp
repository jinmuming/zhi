<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/10/14
  Time: 20:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>忘记密码-知了[团队知识管理]</title>
</head>
<body>
<div class="login-box" id="reset-password">
    <button type="button" data-fancybox-close title="Close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"></path></svg></button>
    <div class="login-box text-center">
        <h1 class="text-white mb-30">忘记密码</h1>

        <form action="#" class="re-form">
            <div class="re-form-group-md">
                <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="用户名">
            </div>
            <div class="re-form-group-md">
                <button class="re-btn re-btn-block re-btn-lg">重置密码</button>
            </div>
        </form>
    </div>
</div>
<c:import url="../template/_include_js.jsp"/>
<style>
    .re-btn{
        border-radius: 3px;
    }
</style>
</body>
</html>
