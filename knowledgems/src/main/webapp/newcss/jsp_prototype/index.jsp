<%--
  Created by IntelliJ IDEA.
  User: 1
  Date: 2021/7/7
  Time: 17:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/mycss.css">
    <style>
        body{background-color: #1b1b1b}
    </style>
</head>
<body>
<div id="login">
    <div class="dx-signin text-center text-white" style="padding-top: 120px;">
        <h1 class="mb-30">登&emsp;录</h1>
        <form action="login" method="post" onsubmit="return preSubmit()" class="dx-form">
            <div class="dx-form-group-md" id="account-div">
                <input type="text" class="form-control" id="account-input" name="account"
                       placeholder="手机号/邮箱" onKeypress="javascript:if(event.keyCode == 32)event.returnValue = false;">
            </div>
            <div class="dx-form-group-md" id="psw-div">
                <input type="password" class="form-control" id="psw-input" name="psw"
                       placeholder="密码">
            </div>
            <div class="dx-form-group-md">
                <button type="submit" class="dx-btn dx-btn-block dx-btn-lg">登录</button>
            </div>
            <div class="dx-form-group-md">
                <div class="d-flex justify-content-between">
                    <div class="d-flex" style="/*实现垂直居中*/align-items: center;color: #b9b9b9">
                        <input type="checkbox" id="rmb-account">&nbsp记住用户
                    </div>
                    <div class="lo-white">
                        <a href="javascript:void(0);" onclick="showForgetLabel()">忘记密码</a>&nbsp|&nbsp<a href="/signup">注册</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
